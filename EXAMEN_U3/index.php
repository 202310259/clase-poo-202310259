<?php
//realizar dos clases, la primera con 3 métodos y cada clase con un atributo,
// segunda clase con 1 método y herecia de la primera clase,y deben usar constructores y destructores//

class FolletoDePrecios{
  
    public function metodo1(){
        echo " Las botas para invierno modelo #64 cuestan $1580 pesos ";
        echo "<br/>";

    }
    private function metodo2(){
        echo "El abrigo de invierno modelo #2 cuesta $2500 ";
        echo "<br/>";
    }
    protected function metodo3(){
        echo "El pantalon modelo #234 cuesta $700";
        echo "<br/>";
    }
    public function AccesoMetodo2(){
        $this->metodo2();
    }
    
    

    }


class Folleto2 extends FolletoDePrecios{
    public function AccesoMetodo3(){
        $this->metodo3();
        
    }

}
$obj = new Folleto2;
$obj->metodo1();
$obj->AccesoMetodo3();
$obj2 = new FolletoDePrecios;
$obj-> AccesoMetodo2();
?>