<?php

class Calculos {


    public function suma(){
        $resultado= 0;
        $n = func_num_args();
        $parametros = func_get_args();
        for ($i=0; $i < $n; $i++) { 
            $resultado+= $parametros[$i];

        }
        return $resultado;
       
    }

    public function PromediosDePesos() {
        $resultado = 0;
        $n = func_num_args();
        $parametros = func_get_args();
        for ($i=0; $i < $n ; $i++) {
            $resultado=$parametros[$i]+$resultado;
        }
        $operacion= $resultado/$n;
        return $operacion;
    }
}
$obj = new Calculos();
echo "La suma consecutiva de las cantidades proporcionadas da como resultado: ". $obj->suma(6,6,3,1);
echo "<br><br>";
echo "El promedio de pesos es el siguiente: ". $obj->PromediosDePesos(56,71,83);

?>