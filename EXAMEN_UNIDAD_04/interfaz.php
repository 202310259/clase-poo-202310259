<?php

interface Procedimientos{
    public function PerimetroDeCuadrado($distancia);
    public function CalcularAltura($distancia);
}
class Calculadora implements Procedimientos{
    public function PerimetroDeCuadrado($distancia){
        return $Perimetro = $distancia*4;
    }
    public function CalcularAltura($distancia){
        return $Altura = $distancia*60/2;

    }
}
$obj = new Calculadora();
echo"<h1>Calculo del área de un cuadrado ".$obj->PerimetroDeCuadrado(12)."</h1>";
echo"<h1>Operación del cálculo de la altura  ".$obj->CalcularAltura(40)."</h1>";

?>