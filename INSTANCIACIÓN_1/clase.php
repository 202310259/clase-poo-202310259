<?php

class Peluquería{
//creamos una propiedad con el comando private, llamemosla "precio"
    private $precio;
//después creamos un constructor al cual le agregamos la propiedad precio
    function __construct($precio){
//después para hacer referencia al atributo precio usaremos el comando this 
        $this->precio = $precio;
    }
//creamos un método llamado preciocorte y retornamos el precio 
    public function preciocorte(){
        return $this->precio;
    }
}



?>