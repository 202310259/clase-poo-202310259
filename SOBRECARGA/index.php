<?php

//CREAMOS LA CLASE OPERACIONES
class Operaciones {


    public function suma(){
        $resultado= 0;
        $n = func_num_args();
        $parametros = func_get_args();
        for ($i=0; $i < $n; $i++) { 
            $resultado+= $parametros[$i];

        }
        return $resultado;
       
    }

    public function calificacionPracticas() {
        $resultado = 0;
        $n = func_num_args();
        $parametros = func_get_args();
        for ($i=0; $i < $n ; $i++) {
            $resultado=$parametros[$i]+$resultado;
        }
        $operacion= $resultado/$n;
        return $operacion;
    }
}

//INSTANCIAMOS LA CLASE FUERA DE ELLA
$obj = new Operaciones();
echo $obj->suma(5,4,2,6);
echo "<br><br>";
echo $obj->calificacionPracticas(80,70,90);



?>