<?php

//creamos una clase
class Modificadores{

}
//creamos los atributos en sus distintos modificadores de acceso a los cuales les agregué un constructor

//el modificador de acceso "public" es para hacer el atributo visible para cualquiera
public $atributo1;

function __construct(){
    echo"  este es el contructor    ";
    $this-> $atributo1 = 1;
}
//el modificador de acceso "private" es para hacer el atributo visible únicamente para la clase que define el miembro
private $atributo2;

function __construct(){
    echo"  este es el contructor    ";
    $this-> $atributo2 = 2;
}
//el modificador de acceso "protected" es para hacer el atributo visible solo dentro de la clase
protected $atributo3;

function __construct(){
    echo"  este es el contructor    ";
    $this-> $atributo3 = 3;
}
//Iniciamos con la implementación de los métodos en sus distintos modificadores de acceso
public function metodo1(){
    $resultado = $a + $b;
    return $resultado
}
private function metodo2(){
    $resultado = $a - $b;
    return $resultado
}
private function metodo3(){
    $resultado = $a * $b;
    return $resultado
}





?>
