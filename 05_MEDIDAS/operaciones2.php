<?php 
//Creamos la clase llamada "Operaciones"

class Operaciones{

//creamos un atributo llamado "operacionarealizar"
    public $operacionrealizar="";

//nuevo métodos creados los cuales corresponderán a opciones que el usuario podrá elegir
    public function demetrosapies($a){
        return "de metros a pies es  ". ($a * 3.281);
    }
    public function demetrosacentimetros($a){
        return $a * 100;
    }
    public function demetrosapulgadas($a){
        return $a * 39.37;
    }

    public function resultadooperacion($a){
        switch($this->operacionrealizar){
            case'demetrosapies':
                return $this->demetrosapies($a, 3.281);
                break; 
            case 'demetrosacentimetros':
                return $this->demetrosacentimetros($a, 100);
                break;
            case 'demetrosapulgadas':
                return $this->demetrosapulgadas($a, 39.37);
                break;

            default:
            return'Operación a realizar no definida';
            break;
            
        }
    }
}


?>